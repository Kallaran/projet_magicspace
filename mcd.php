
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
               "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>MCD</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="script.js"></script>
<link rel="icon" type="image/png" href="images/favicon.png" />
</head>

<body>

	<?php include "menu.php" ?>
	<div id="fond1"></div>
	<div class="centrer">


	   <br />
    <br />

	<div class="t"> MCD, MPD et MLRD réalisés avec <a  href="https://launchpad.net/analysesi">AnalyseSI</a>. Un outil permettant de modéliser une base de données avec la méthode MERISE.    </div>
   <br />
    <br />
<h3>MODELE CONCEPTUEL DE DONNEES</h3>

<div class="fblanc">

    <img class="cen" src="images/mcd.png" alt="mcd" />
    
    </div>

	<br />
    <br />

<h3>MODELE PHYSIQUE DE DONNEES</h3>

<div class="fblanc">

    <img class="cen" src="images/mpd.png" alt="mpd" />
	
	    </div>

   <br />
    <br />
	
	<h3>MODELE RELATIONNEL DE DONNEES</h3>
	
	
	<div class="fblanc2">

	    <img class="cen" src="images/mldr.png" alt="mpd" />
	    </div>

	   <br />
    <br />
	   <br />
    <br />
	   <br />
    <br />
   <br />
    <br />
	   <br />
    <br />

</div>
</body>

</div>
