
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
               "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>TABLES</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="script.js"></script>
<link rel="icon" type="image/png" href="images/favicon.png" />
</head>

<body>


	<?php include "menu.php" ?>
	<div id="fond1"></div>
	<div class="centrer">

	
	
	<h3>JOUEUR</h3>

<div class="t">  La table Joueur, composée d'une clé primaire 'Nom' et des attributs 'Age' et 'Ville'.</div>
	
	<br />
<div class="r"> SELECT * FROM Joueur</div>
    <br />
    <br />
	
	<div id="req">
	<?php include "tables/joueur.php" ?>
	</div>
	
	<br />
	<br />

	
	
	
	<h3>EDITION</h3>

<div class="t"> La table Edition, composée d'une clé primaire 'Nom' et de l'attribut 'Année'.</div>
	
	<br />
<div class="r"> SELECT * FROM Edition</div>
    <br />
    <br />
	
	<div id="req">
	<?php include "tables/edition.php" ?>
	</div>
	
	<br />
	<br />
	
	
	
	
	<h3>CARTE</h3>

<div class="t">  La table Carte, composée d'une clé primaire 'IDC'. Ainsi que des clés étrangères 'Proprietaire' référençant 'Nom' de Joueur, 'Edition' référençant 'Nom' de Edition, 'Artiste' référençant 'Nom' de Artiste. Cette table contient également les attributs 'Carte' et 'Valeur'.</div>
	
	<br />
<div class="r"> SELECT * FROM Carte</div>
    <br />
    <br />
	
	<div id="req">
	<?php include "tables/carte.php" ?>
	</div>
	
	<br />
	<br />
	
	
<h3>ARTISTE</h3>

<div class="t"> La table Artiste, composée d'une clé primaire 'Nom' et des attributs 'Ville' et 'Numero'. </div>
	
<br />
<div class="r"> SELECT * FROM Artiste</div>
<br />
<br />
	
	<div id="req">
	<?php include "tables/artiste.php" ?>
	</div>
	
	<br />
	<br />
	

	
	
	
	


	<h3>CONTENIR</h3>

<div class="t"> lorem</div>
	
	<br />
<div class="r"> SELECT * FROM Contenir</div>
    <br />
    <br />

<div id="req">
	<?php include "tables/contenir.php" ?>
	</div>
	
	<br />
	<br />



</div>
</body>

