<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
               "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta charset="UTF-8">

<title>MAGIC THE GATHERING</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="script.js"></script>
<link rel="icon" type="image/png" href="images/favicon.png" />

</head>

<body>



<?php include "menu.php" ?>








<div id="videoDiv"> 

<div id="videoBlock">   
<video preload="preload" muted id="video" autoplay="autoplay" loop="loop">
<source src="videos/lore_5.mp4" type="video/mp4" />
</video> 


<div id="videoMessage">
<div >
<h1 class="">Découvrez Magic,</h1>
<h1 class="">Un domaine tumultueux ! </h1>
</div>





</div>

</div>
</div>





<!--
<div id="videoDiv"> 

<div id="videoBlock">   
<video preload="preload" muted id="video" autoplay="autoplay" loop="loop">
<source src="videos/lore_5.mp4" type="video/mp4"></source>
</video> 


<div id="videoMessage">
<h1 class="spacer">The Early Bird...</h1>
<h2 class="spacer">...has the best holiday.</h2>
<h3 class="spacer">Boat rentals in France</h3>
<p class="videoClick" >
<a href="https://www.hotelsafloat.com/home-away.php">Click here and be impressed</a>
</p> 
</div>
</div>
</div>


-->








<div class="centrer">
	
		

	<h3>Un jeu pas tout jeune ...</h3>

	<div class="im1">
	<p class="t gau tt" > Magic the Gathering est un Jeu de Cartes à Collectionner édité par la société américaine Wizards of the Coast depuis
	 août 1993. Magic the Gathering met en scène une joute de deux magiciens (appelés Arpenteurs des plans) 
	 commandant aux éléments dans un univers médiéval fantastique. </p>



	<img class="" src="images/jace.jpg" alt="jace" />

	
	</div>
	
	
	
		<br />

	
	<h3>De nombreux univers et une histoire sans limite !</h3>

	<div class="im2">
	
	<img class="gaui" src="images/gob.jpg" alt="gob" />

	<p class="t dro tt">Si les jeux de cartes à collectionner actuels se basent sur un univers fictionnel déjà existant (Le Seigneur des anneaux, Pokémon, etc.) ; la particularité de Magic réside dans le fait que son propre univers est développé 
	au travers des cartes édités. Cet univers a donné naissance à des romans et comics qui mettent en scène les héros que l'on peut retrouver dans les illustrations et les textes d'ambiance du jeu.
	</div>
	<br />

	<h3>Une communauté gigantesque !</h3>


<div class="im3">

	
		<img class="droi" src="images/auto.jpg" alt="auto" />


	<p class="t gau tt">De part son grand âge et sa capacité à se renouveler, Magic the Gathering a su plaire et créer au fil des années une communauté gigantesque de plusieurs dizaines de millions de joueurs. </p>
</div>






	
</div>
	<img id="tcool" src="images/tcool.png" alt="tcool" />			

	<img id="tnissa" src="images/tnissa.png" alt="tnissa" />			

</body>
</html>

