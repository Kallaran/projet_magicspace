﻿<?php
ini_set('display_errors', 1);
error_reporting(-1);

try{
$conn = new PDO('mysql:host=mysql.istic.univ-rennes1.fr;dbname=base_16002266','user_16002266','rybwmw');
	$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch(PDOExcepion $e){
echo "Erreur de connecion : " . $e->getMessage();
}
$xml = new SimpleXMLElement('<root/>');

$tableArtiste=$conn->query('SELECT * FROM `Artiste`');
foreach ($tableArtiste as $line) {
	$courant = $xml->addChild("Artiste");
	$courant->addAttribute('Nom', $line['Nom']);
	$courant->addChild('Ville',$line['Ville']);
	$courant->addChild('Numero',$line['Numero']);
}

$tableEdition=$conn->query('SELECT * FROM `Edition`');
foreach ($tableEdition as $line) {
	$courant = $xml->addChild("Edition");
	$courant->addAttribute('Nom', utf8_encode($line['Nom']));
	$courant->addChild('Annee',$line['Annee']);
}

$tableJoueur=$conn->query('SELECT * FROM `Joueur`');
foreach ($tableJoueur as $line) {
	$courant = $xml->addChild("Joueur");
	$courant->addAttribute('Nom', utf8_encode($line['Nom']));
	$courant->addChild('Age',$line['Age']);
	$courant->addChild('Ville',$line['Ville']);

}

$tableCarte=$conn->query('SELECT * FROM `Carte`');
foreach ($tableCarte as $line) {
	$courant = $xml->addChild("Carte");
	$courant->addAttribute('IDC', $line['IDC']);
	$courant->addAttribute('Edition', utf8_encode($line['Edition']));
	$courant->addAttribute('Artiste', utf8_encode($line['Artiste']));
	$courant->addAttribute('Carte', utf8_encode($line['Carte']));
	$courant->addChild('Proprietaire', utf8_encode($line['Proprietaire']));
	$courant->addChild('Valeur',$line['Valeur']);
}

$tableContenir=$conn->query('SELECT * FROM `Contenir`');
foreach ($tableContenir as $line) {
	$courant = $xml->addChild("Contenir");
	$courant->addChild('IDC', $line['IDC']);
	$courant->addChild('Edition', utf8_encode($line['Edition']));
}



Header('Content-type: text/xml');
print($xml->asXML());
?>


	
	
