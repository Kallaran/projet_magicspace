<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
               "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>REQUETES</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="script.js"></script>
<link rel="icon" type="image/png" href="images/favicon.png" />
</head>

<body>
	
	
	<?php include "menu.php" ?>
	<div id="fond1"></div>
<div class="centrer">



	

	<h3>SELECTION AVEC PROJECTION</h3>
	
	<div class="t"> Sélection avec projection des éditions sorties après 2003.</div>
	
	<br />
   <div class="r"> SELECT * FROM Edition WHERE Annee >= 2003</div>
    <br />
    <br />
    
	<div id="req">
	<?php include "requetes/1.php" ?>
	</div>
	
	<br />
	<br />
	
	<h3>JOINTURE</h3>
	
		<div class="t"> On souhaite obtenir les numéros et noms des artistes ayant travaillés sur les cartes de l'édition "Kaladesh". </div>
	
	<br />
   <div class="r"> SELECT DISTINCT Artiste.Nom, Numero FROM Artiste, Carte WHERE Carte.Artiste = Artiste.Nom and Edition ='Kaladesh'</div>
    <br />
    <br />
    
	<div id="req">
	<?php include "requetes/2.php" ?>
	</div>
	
	<br />
	<br />
	
	<h3>MOYENNE SUR L'INTEGRALITE D'UN ATTRIBUT</h3>
	
<div class="t">Calcul de la moyenne de l'âge des joueurs.</div>
    <br />
   <div class="r"> SELECT AVG(Age) FROM Joueur</div>
    <br />
    <br />
	<?php include "requetes/3.php" ?>
	
	<br />
	<br />
	
	<h3>REGROUPEMENT AVEC CALCUL</h3>
	
<div class="t"> Calcul de la somme des valeurs des cartes de chaque joueur.</div>
    <br />
   <div class="r"> SELECT Proprietaire, SUM(Valeur) AS 'Somme' FROM Carte GROUP BY Proprietaire ORDER BY Proprietaire</div>
    <br />
    <br />
	<?php include "requetes/4.php" ?>
	
	<br />
	<br />
	
	<h3>DIFFERENCE</h3>
	
	<div class="t">Sélection des cartes que Jean et Louis ne possèdent pas, avec leur valeur et le nom de leur propriétaire.</div>
    <br />
   <div class="r">SELECT Carte, Valeur, Proprietaire from Carte WHERE Proprietaire NOT IN (SELECT Proprietaire FROM Carte WHERE Proprietaire = 'Jean' OR Proprietaire = 'Louis')</div>
    <br />
    <br />
	<?php include "requetes/5.php" ?>
	
	<br />
	<br />
	
	<h3>DIVISION</h3>
	
	<div class="t">Sélection des cartes éditées après 2012.</div>
    <br />
    <div class="r">SELECT Carte FROM Carte WHERE Edition IN (SELECT Nom FROM Edition WHERE Annee >= 2012)</div>
    <br />
    <br />
    
	<?php include "requetes/6.php" ?>
	
	<br />
	<br />
	<br />
	<br />
</div>
</body>
</html>
